[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6798318.svg)](https://doi.org/10.5281/zenodo.6798318)
[![GNU GPL v3 license](https://img.shields.io/badge/license-GPLv3-green)](http://www.gnu.org/licenses/)
[![Open Database v1.0 license](https://img.shields.io/badge/license-ODbL-green)](https://opendatacommons.org/licenses/odbl/1-0/)



# Annotating symbolic texture in piano music: a formal syntax

Repository of the paper: Louis Couturier, Louis Bigo, Florence Levé, *Annotating symbolic texture in piano music: a formal syntax*, Sound and Music Computing (SMC 2022), Saint-Etienne, June 2022. [link](https://doi.org/10.5281/zenodo.6798318)

This repository includes:
- `textureclass.py`: defines the classes `Layer` and `Texture` used to model textural configurations and parse textural labels.
  - `tests/test_textureclass.py`: unit tests for each method of `Layer` and `Texture`; ensures validity of the parsing according to the proposed syntax for texture annotation.
- `bestiary_of_musical_textures.mscz`, version 3.2: Compendium of annotated textures. The file is to be opened with [Musescore3](https://musescore.org). 
- `mozart_K280-3.txt`: textural annotations in the *Presto* of Mozart Piano Sonata n°2 (K.280/189e). The score can be found [here](https://github.com/DCMLab/mozart_piano_sonatas/tree/main/pdf).

## The syntax

The implemented syntax is defined in Backus Naur Form as follows:
```
<bar-texture> ::= <texture> [',' <texture>]   # Label for a single bar
<texture>     ::= <description> '[' <layer-list> ']'  # Detailed
                | <layer-list>                        # Shortcut
                | _                                   # Silence
<layer-list>  ::= <layer> ['/' <layer-list>]
<layer>       ::=   <layer-function><description> ['(' <layer-list> ')']
<description> ::=   <density> [<relation>][<attribute-set>]
<attribute-set>  ::=  <attribute> [<attribute-set>]
<layer-function> ::=  M | H | S | MH | MS | HS | MHS | N
<relation>    ::=  h | p | o         # homorhythmy, parallel motions, octave motions
<attribute>   ::=  s | t | b | r | _ # scale, sustained, oscillation, repetition, sparse
<density>     ::=  .. integer in N* ..
```

## Authors and acknowledgement

Louis Couturier, Louis Bigo, Florence Levé from MIS, Université de Picardie Jules Verne, and CRIStAL, UMR 9189 CNRS, Université de Lille, France.

Contact: louis.couturier@u-picardie.fr

## License

The code is licensed under the GNU General Public License v3.0 or any later version. The data (bestiary, annotations) are licensed under the Open Database License. Any rights in individual contents of the database are licensed under the Database Contents License.
