"""
This file is part of "Symbolic Texture Piano" http://algomus.fr/code 
Copyright (C) 2021-2022 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Piano" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Piano" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Piano". If not, see
http://www.gnu.org/licenses/
"""


import sys
import unittest
sys.path.append('../')

from textureclass import Layer, Texture



class TestLayer(unittest.TestCase):
    #def setUp(self):
    #    pass

    def test_layer_init(self):
        layer = Layer("MH4h")
        # Check attributes
        self.assertEqual(layer.notation, 'MH4h')
        self.assertEqual(layer.function, 'MH')
        self.assertEqual(layer.density, 4)
        self.assertEqual(layer.attributes, ['h'])
        self.assertEqual(len(layer), 1)

        # Check unvalid notation
        with self.assertRaises(ValueError):
            layer = Layer("4h") # Missing function
        with self.assertRaises(ValueError):
            layer = Layer("MHh") # Missing density
        with self.assertRaises(ValueError):
            layer = Layer("MPA4o") # Unknown function
        with self.assertRaises(ValueError):
            layer = Layer("MH4bla") # Unknown attributes
        with self.assertRaises(ValueError):
            layer = Layer("blabla") # Unvalid syntax

    def test_silence_init(self):
        layer = Layer("_")
        # Check attributes
        self.assertEqual(layer.notation, '_')
        self.assertEqual(layer.function, 'N')
        self.assertEqual(layer.density, 0)
        self.assertEqual(layer.attributes, [])
        self.assertEqual(len(layer), 0)

    def test_type_conversion(self):
        # Full layer
        layer = Layer("MH4h")
        self.assertEqual(str(layer), 'MH4h')
        self.assertEqual(int(layer), 4)
        self.assertTrue(bool(layer))

        # Silence
        layer = Layer("_")
        self.assertEqual(str(layer), '_')
        self.assertEqual(int(layer), 0)
        self.assertFalse(bool(layer))

    def test_eq(self):
        self.assertTrue(Layer("_") == Layer("_"))
        self.assertTrue(Layer("MH4h") == Layer("MH4h"))
        self.assertFalse(Layer("_") == Layer("MH4h"))

        self.assertTrue(Layer("_") != Layer("MH4h"))
        self.assertFalse(Layer("MH4h") != Layer("MH4h"))

    def test_is_melodic(self):
        self.assertTrue(Layer("MH4h").is_melodic())
        self.assertFalse(Layer("S4").is_melodic())

    def test_is_harmonic(self):
        self.assertTrue(Layer("MH4h").is_harmonic())
        self.assertFalse(Layer("S4").is_harmonic())

    def test_is_support(self):
        self.assertTrue(Layer("S4").is_support())
        self.assertFalse(Layer("MH4h").is_support())

    def test_is_sparse(self):
        self.assertTrue(Layer("MH4h_").is_sparse())
        self.assertFalse(Layer("MH4h").is_sparse())

    def test_standard_notation(self):
        # Already standard notation
        self.assertTrue(Layer.is_standard_notation("MH4h"))
        self.assertEqual(Layer.standard_notation("MH4h"), "MH4h")

        # To standardize
        self.assertFalse(Layer.is_standard_notation("HM4h"))
        self.assertEqual(Layer.standard_notation("HM4h"), "MH4h")

        # Unvalid notation
        self.assertFalse(Layer.is_standard_notation("bla"))
        self.assertEqual(Layer.standard_notation("bla"), "")

    def test_is_terminal(self):
        self.assertTrue(Layer("_").is_terminal())
        self.assertTrue(Layer("MH4h").is_terminal())
        self.assertFalse(Layer("HS1(M1/S1)").is_terminal())
        self.assertFalse(Layer("M1(M1/HS1(M1/S1))").is_terminal())

    def test_high_level(self):
        self.assertEqual(Layer("_").high_level(), "_")
        self.assertEqual(Layer("MH4h").high_level(), "MH4h")
        self.assertEqual(Layer("HS1(M1/S1)").high_level(), "HS1")
        self.assertEqual(Layer("M1(M1/HS1(M1/S1))").high_level(), "M1")

    def test_depth(self):
        self.assertEqual(Layer("_").depth(), 1)
        self.assertEqual(Layer("MH4h").depth(), 1)
        self.assertEqual(Layer("HS1(M1/S1)").depth(), 2)
        self.assertEqual(Layer("M1(M1/HS1(M1/S1))").depth(), 3)


class TestTexture(unittest.TestCase):
    def test_texture_init(self):
        # Single layer
        texture = Texture("MH4h")
        self.assertEqual(texture.notation, "MH4h")
        self.assertEqual(texture.global_density, 4)
        self.assertEqual(texture.global_attributes, ['h'])
        self.assertEqual(len(texture), 1)

        # Multiple layer
        texture = Texture("M1s/HS1") # shortcut notation for 2[M1s/HS1]
        self.assertEqual(texture.notation, "2[M1s/HS1]")
        self.assertEqual(texture.global_density, 2)
        self.assertEqual(texture.global_attributes, [])
        self.assertEqual(len(texture), 2)

        # Multiple layer with high-level density
        texture = Texture("2[M1/H2h]")
        self.assertEqual(texture.notation, "2[M1/H2h]")
        self.assertEqual(texture.global_density, 2)
        self.assertEqual(texture.global_attributes, [])
        self.assertEqual(len(texture), 2)

        # With recursive definition
        texture = Texture("M1s/HS1(S1/M1)")
        self.assertEqual(texture.notation, "2[M1s/HS1(S1/M1)]")
        self.assertEqual(texture.global_density, 2)
        self.assertEqual(texture.global_attributes, [])
        self.assertEqual(len(texture), 2)

        # Check unvalid notation
        with self.assertRaises(ValueError):
            texture = Texture("4h") # Missing function
        with self.assertRaises(ValueError):
            texture = Texture("/MH4") # Empty layer
        with self.assertRaises(ValueError):
            texture = Texture("(MH4") # Unclosed parenthesis
        with self.assertRaises(ValueError):
            texture = Texture("(MH4)") # Unnecessary parenthesis

    def test_silence_init(self):
        texture = Texture("_")
        # Check attributes
        self.assertEqual(texture.notation, "_")
        self.assertEqual(texture.global_density, 0)
        self.assertEqual(texture.global_attributes, [])
        self.assertEqual(len(texture.layers), 1)

    def test_type_conversion(self):
        # Silence
        texture = Texture("_")
        self.assertEqual(str(texture), '_')
        self.assertEqual(int(texture), 0)
        self.assertFalse(bool(texture))

        # Single Layer
        texture = Texture("MH4h")
        self.assertEqual(str(texture), 'MH4h')
        self.assertEqual(int(texture), 4)
        self.assertTrue(bool(texture))

        # Multi Layer
        texture = Texture("M2p/MH4h")
        self.assertEqual(str(texture), '6[M2p/MH4h]')
        self.assertEqual(int(texture), 6)
        self.assertTrue(bool(texture))

    def test_eq(self):
        self.assertTrue(Texture("_") == Texture("_"))
        self.assertTrue(Texture("M1s/HS1") == Texture("M1s/HS1"))
        self.assertFalse(Texture("_") == Texture("M1s/HS1"))

        self.assertTrue(Texture("_") != Texture("M1s/HS1"))
        self.assertFalse(Texture("M1s/HS1") != Texture("M1s/HS1"))

    def test_copy(self):
        texture = Texture("2[M1/H2h]")
        texture_copy = texture.copy()
        self.assertEqual(texture.notation, texture_copy.notation)
        self.assertEqual(texture.global_density, texture_copy.global_density)
        self.assertEqual(texture.global_attributes, texture_copy.global_attributes)
        self.assertEqual(len(texture), len(texture_copy))

    def test_has_melodic(self):
        self.assertTrue(Texture("M1s/HS1").has_melodic())
        self.assertFalse(Texture("H4/S2o").has_melodic())

    def test_has_harmonic(self):
        self.assertTrue(Texture("M1s/HS1").has_harmonic())
        self.assertFalse(Texture("M1/S2o").has_harmonic())

    def test_has_support(self):
        self.assertTrue(Texture("M1s/HS1").has_support())
        self.assertFalse(Texture("M1/H2h").has_support())

    def test_is_monophonic(self):
        self.assertTrue(Texture("MH4h").is_monophonic())
        self.assertFalse(Texture("M1s/HS1").is_monophonic())

    def test_is_polyphonic(self):
        self.assertTrue(Texture("4[M1/M1/M1/S2o]").is_polyphonic())
        self.assertFalse(Texture("MH4h").is_polyphonic())

    def test_init_sequential(self):
        seq_texture = Texture("HS4h, _")
        self.assertEqual(str(seq_texture), "HS4h, _")
        self.assertEqual(seq_texture.sequential, Texture("_"))

        seq_texture = Texture("M1, M2, M3")
        self.assertEqual(str(seq_texture), "M1, M2, M3")
        self.assertEqual(str(seq_texture.sequential), "M2, M3")
        self.assertEqual(str(seq_texture.sequential.sequential), "M3")

    def test_is_sequential(self):
        self.assertTrue(Texture("HS4h, _").is_sequential())
        self.assertTrue(Texture("M1, M2, M3").is_sequential())
        self.assertFalse(Texture("M1s/HS2").is_sequential())

    def test_is_homophonic(self):
        self.assertTrue(Texture("M1s/HS2").is_homophonic())
        self.assertTrue(Texture("MH4h").is_homophonic())
        self.assertFalse(Texture("M1/S1").is_homophonic())
        self.assertFalse(Texture("H1/S1t").is_homophonic())
        self.assertFalse(Texture("S2o").is_homophonic())

    def test_depth(self):
        self.assertEqual(Texture("_").depth(), 1)
        self.assertEqual(Texture("M1/MH4h").depth(), 1)
        self.assertEqual(Texture("M1/HS1(M1/S1)").depth(), 2)
        self.assertEqual(Texture("M1/M1(M1/HS1(M1/S1))").depth(), 3)

    def test_child_texture(self):
        self.assertEqual(
            str(Texture("_").child_texture()),
            "_")
        self.assertEqual(
            str(Texture("MH4h").child_texture()),
            "MH4h")
        self.assertEqual(
            str(Texture("M1/HS1(M1/S1)").child_texture()),
            "2[M1/M1/S1]")
        self.assertEqual(
            str(Texture("M1/M1(M1/HS1(M1/S1))").child_texture()),
            "2[M1/M1/HS1(M1/S1)]")

    # Additional test
    #def test_tree(self):
    #    Texture("MH4h").tree()
    #    Texture("M1/HS1(M1/S1)").tree()
    #    Texture("M1/M1(M1/HS1(M1/S1))").tree()



if __name__ == '__main__':
    unittest.main()
