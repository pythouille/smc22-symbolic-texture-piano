"""
This file is part of "Symbolic Texture Piano" http://algomus.fr/code 
Copyright (C) 2021-2022 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Symbolic Texture Piano" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Symbolic Texture Piano" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Symbolic Texture Piano". If not, see
http://www.gnu.org/licenses/
"""


import re

try:
    import matplotlib.pyplot as plt
    HAS_MATPLOTLIB = True
except ModuleNotFoundError:
    print("matplotlib module not found.")
    HAS_MATPLOTLIB = False



LAYER_FUNCTION_UNITS = ["M", "H", "S", "N"]
LAYER_FUNCTIONS = [
    "M", "H", "S", # Melodic, Harmonic, Static/Rhythmic
    "MH", "MS", "HS", "MHS", # Mixed
    "N" # None of the above
]
RELATIONS = [
    'h', # homorhythmy
    'p', # parallel motions
    'o', # octave
    #'u',  # unison
]
ATTRIBUTES = [
    's', # scale
    't', # sustained notes ('tenu')
    'b', # oscillation ('battement'): oscillation between two notes
    'r', # repetition: notes or chords are repeated at least twice
    '_', # sparse: low horizontal density
    #'a', # arpeggio
]



class Layer:
    """Class to describe a textural layer or sublayer.

    It models:
    - Function of the layer: M, H, S (melodic, harmonic, support) 
      or combinations
    - Density of the layer: integer
    - Relationships between voices: h, p, o
    - Other attributes: s, t, b, r, _ 
      (scale, sustained, oscillation, repetition, sparse)
    """
    def __init__(self, notation="_", verbose=1):
        self.attributes = []
        self.sublayers = []

        ## Regular expression:
        # Group 1: function(s) M|H|S|MH|... (required)
        # Group 2: superfluous functions
        # Group 3: density: integer value (required)
        # Group 4: other attributes: h, p, o, s,...
        # Group 5: recursive decomposition
        # Group 6: superfluous characters
        regex_match = re.match(
            r"^([M|H|S|N]*)([A-Z]*)(\d*)([hpousatbr_]*)(\(.+\))?(\w*)$",
            notation
        )

        if notation == "_": # silent layer
            self.function = "N"
            self.density = 0
            self.notation = notation
        elif regex_match:
            ## Check annotation validity
            # Add function (M/H/S)
            if not regex_match.group(1):
                raise ValueError(
                    "Missing layer function in '{}'. Layer annotation must"\
                    " begin with a function in {}".format(
                        notation, LAYER_FUNCTIONS)
                )
            else:
                self.function = ""
                # Rebuild function label in standardized order
                for function in LAYER_FUNCTION_UNITS:
                    if function in regex_match.group(1):
                        self.function += function
                if (regex_match.group(2) or
                    self.function not in LAYER_FUNCTIONS):
                    raise ValueError(
                        "Unvalid layer function in '{}'. Layer annotation"\
                        " must begin with a function in {}".format(
                            notation, LAYER_FUNCTIONS)
                    )
            # Add density
            if not regex_match.group(3):
                raise ValueError(
                    "Missing density value in '{}'. Layer annotation"\
                    " must contain an integer value for the vertical "\
                    "density.".format(notation)
                )
            else:
                self.density = int(regex_match.group(3))
            
            # Add textural attributes
            if regex_match.group(6):
                raise ValueError(
                    "Unknown identifier in given annotation in "\
                    "'{}' : '{}'".format(notation, regex_match.group(6))
                )
            else:
                # Rebuild the list of attributes in standardized order
                for attribute in RELATIONS+ATTRIBUTES:
                    if attribute in regex_match.group(4):
                        self.attributes.append(attribute)

            # Add sublayers (recursive description)
            if regex_match.group(5):
                subparts = regex_match.group(5)[1:-1].split("/")
                sublayer = ""
                for subpart in subparts:
                    # Rebuild sublayers
                    # For example : (H2(S1/M1)/M1) will split H2(S1 and M1)
                    #  that need to be merged back in H2(S1/M1)
                    sublayer += "/" + subpart
                    if sublayer.count('(') - sublayer.count(')') == 0:
                        # The layer notation is complete
                        self.sublayers.append(Layer(sublayer[1:], verbose=0))
                        sublayer = ""
                    # else: The layer needs at least the next subpart to be defined

            # Define notation
            self.notation = (
                self.function
                + str(self.density)
                + ''.join(self.attributes))
            if self.sublayers:
                self.notation += (
                    '(' + '/'.join(
                        [str(sublayer) for sublayer in self.sublayers]
                    ) + ')')
            if verbose and notation != self.notation:
                print("Corrected notation syntax from "\
                      "'{}' to '{}'".format(notation, self.notation)
                )
        else:
            raise ValueError(
                "Invalid string argument : '{}'".format(notation))

    def __repr__(self):
        return self.notation

    def __len__(self):
        """
        Return the number of sublayers that compose this layer 
        (diversity of the layer)
        """
        if not self:
            return 0 # silent layer
        if not self.sublayers:
            return 1 # default description of a layer
        # else, count the sublayers
        return len([l for l in self.sublayers if l])

    def __int__(self):
        """
        Return the density of the layer, i.e. the estimated number 
        of simultaneous voices
        """
        return self.density

    def __bool__(self):
        """Return False if the layer is only silence, else True"""
        return self.notation != "_"

    def __eq__(self, other):
        return self.notation == other.notation

    @classmethod
    def standard_notation(cls, notation):
        """Return standardized version of given layer notation.

        Empty string is returned if given notation is unvalid
        """
        try:
            test_layer = Layer(notation, verbose=0)
        except ValueError:
            return "" # Empty string if unvalid
        return test_layer.notation

    @classmethod
    def is_standard_notation(cls, notation):
        """Return True if given notation is a standard layer notation"""
        return (
            notation and # non-empty
            notation == Layer.standard_notation(notation) # standard notation
        )

    def is_melodic(self):
        """Return True if the layer has a melodic function"""
        return 'M' in self.function

    def is_harmonic(self):
        """Return True if the layer has a harmonic function"""
        return 'H' in self.function

    def is_support(self):
        """Return True if the layer has a static function
        """
        return 'S' in self.function

    def has_attribute(self, attribute, high_level_only=False):
        """Return True if given attribute is present in the layer
        or in one of its sublayers
        
        Parameters
        ----------
        attribute: str
            chosen in :
            'h': homorhythmy
            'p': parallel motions
            'o': octave motions
            's': scale
            't': sustained notes
            'b': oscillation between two notes
            'r': repetition; notes or chords repeated at least twice
            '_': sparsity; low horizontal density
        
        high_level_only: bool
            If True, look for given attribute only in the first level
            of description of the layer, and not in its sublayers.
            Defaults to False.
        """
        # Check global attributes
        if attribute in self.attributes:
            return True
        if not high_level_only:
            # Check in sublayers
            for sublayer in self.sublayers:
                if sublayer.has_attribute(attribute):
                    return True
        return False

    def is_sparse(self):
        """Return True if the layer is sparse:
        i.e. with low horizontal density
        """
        return '_' in self.attributes

    def high_level(self):
        """Return the high-level description of the layer,
        i.e. without considering sublayers
        """
        if not self:
            return "_"
        return self.function + str(self.density) + ''.join(self.attributes)

    def is_terminal(self):
        """Return True if the layer has no sublayers"""
        return not bool(self.sublayers)

    def depth(self):
        """Return the maximal depth of sublayers in the layer"""
        if self.is_terminal():
            return 1
        return max([sublayer.depth()+1 for sublayer in self.sublayers])


class Texture:
    """Class to describe symbolic texture of a musical extract"""
    def __init__(self, notation="_"):
        # Check sequentiality of the texture
        if ',' in notation:
            split_texture = notation.replace(' ','').split(',')
            notation = split_texture[0] # current texture
            self.sequential = Texture( # next texture
                ','.join(split_texture[1:]))
        else:
            self.sequential = None

        # Initialize attributes
        self.global_density = 0
        self.global_attributes = []
        self.layers = []

        # Check <density><attr>[<layers>]
        regex_match = re.match(r"^(\d+)(\w*)\[(.*)\]$", notation)
        tmp_global_density = None
        if regex_match:
            ## Case with high-level density indication (detailed notation)
            # Retrieve global density
            tmp_global_density = int(regex_match.group(1))
            # Retrieve global attributes
            for attribute in RELATIONS+ATTRIBUTES:
                if attribute in regex_match.group(2):
                    self.global_attributes.append(attribute)
            # Retrieve layers
            layers_str = regex_match.group(3)
        else:
            ## (shortcut notation)
            layers_str = notation

        ## Parse the successive layers
        begin_index = 0
        current_depth = 0
        for char_counter in range(len(layers_str)):
            if layers_str[char_counter] == '(':
                current_depth += 1
            elif layers_str[char_counter] == ')':
                current_depth -= 1
            elif layers_str[char_counter] == '/' and not current_depth:
                layer = Layer( # Build layer
                    layers_str[begin_index:char_counter])
                begin_index = char_counter + 1
                self.layers.append(layer)
                # By default, global density is the sum of all layers' density
                self.global_density += layer.density
        # Add last layer
        layer = Layer(layers_str[begin_index:])
        self.layers.append(layer)
        # Set global density
        if tmp_global_density:
            self.global_density = tmp_global_density
        else:
            self.global_density += layer.density
        if current_depth != 0:
            raise ValueError("Erroneous use of parentheses.")

        # Set standardized notation
        if notation == "_":
            self.notation = notation
        elif len(self.layers) == 1:
            self.global_attributes = self.layers[0].attributes
            self.notation = self.layers[0].notation
        else:
            self.notation = (
                str(self.global_density)
                + ''.join(self.global_attributes)
                + '[' + '/'.join([str(layer) for layer in self.layers]) + ']'
            )
        if self.sequential is not None:
            self.notation += ", " + str(self.sequential)

    def __repr__(self):
        return self.notation

    def __len__(self):
        """Return the number of top-level layers"""
        return len([l for l in self.layers if l])

    def __int__(self):
        """Return the global density, i.e. the estimated number
        of simultaneous voices
        """
        return self.global_density

    def __float__(self):
        """Return the global density, ie the estimated number
        of simultaneous voices

        If the texture is sequential, it returns the average of the
        two successive textures.
        """
        if self.is_sequential():
            return float((self.global_density + self.sequential.global_density)/2)
        return float(self.global_density)

    def __bool__(self):
        """Return False if the texture is only silence, else True"""
        return self.notation != "_"

    def __eq__(self, other):
        return self.notation == other.notation

    def copy(self):
        """Return a copy of current texture"""
        return Texture(self.notation)

    def has_melodic(self):
        """Return True if the texture has at least one melodic layer"""
        for layer in self.layers:
            if layer.is_melodic():
                return True
        return False

    def has_harmonic(self):
        """Return True if the texture has at least one harmonic layer"""
        for layer in self.layers:
            if layer.is_harmonic():
                return True
        return False

    def has_support(self):
        """Return True if the texture has at least one supporting layer"""
        for layer in self.layers:
            if layer.is_support():
                return True
        return False

    def has_attribute(self, attribute, high_level_only=False):
        """Return True if given attribute is present in the texture
        or in one of its layers. Otherwise, return False.
        
        Parameters
        ----------
        attribute: str
            chosen in :
            'h': homorhythmy
            'p': parallel motions
            'o': octave motions
            's': scale
            't': sustained notes
            'b': oscillation between two notes
            'r': repetition; notes or chords repeated at least twice
            '_': sparsity; low horizontal density
        
        high_level_only: bool
            If True, look for given attribute only in the first level
            of description of the layers, and not in the sublayers.
            Defaults to False.
        """
        if attribute not in ATTRIBUTES+RELATIONS:
            print("WARNING: unknown attribute:", attribute)
        # Check global attributes
        if attribute in self.global_attributes:
            return True
        # Check in layers and sublayers
        for layer in self.layers:
            if layer.has_attribute(attribute,
                    high_level_only=high_level_only):
                return True
        # Check in sequential texture
        if self.is_sequential():
            if attribute in self.sequential.global_attributes:
                return True
            for layer in self.sequential.layers:
                if layer.has_attribute(attribute,
                        high_level_only=high_level_only):
                    return True
        # Otherwise, the attribute is not present
        return False

    def is_monophonic(self):
        """Return True if the texture consists in a
        unique non-silent layer
        """
        return bool(self) and len(self.layers) == 1

    def is_polyphonic(self):
        """Return True if the texture has two or more
        melodic-only layers
        """
        return len([l for l in self.layers if l.function == 'M']) > 1
    
    def is_homophonic(self):
        """Return True if the texture has at least a melodic
        and a harmonic layer
        """
        return self.has_melodic() and self.has_harmonic()

    def is_sequential(self):
        """Return True if the texture is sequential"""
        return self.sequential is not None

    def has_homorhythmy(self):
        """
        Return True if the texture contains homorhythmy, and False
        otherwise. It includes 'pure' homorhythmy 'h', but also parallel
        and octave motions ('p' and 'o') which are also homorhythmic.
        """
        # Check global attributes
        for attribute in ['h', 'p', 'o']:
            if attribute in self.global_attributes:
                return True
            # Check in layers and sublayers
            for layer in self.layers:
                if layer.has_attribute(attribute,
                        high_level_only=False):
                    return True
        # Else, no homorhythmy found
        return False

    def has_parallel_motions(self):
        """
        Return True if the texture contains parallelism, and False
        otherwise. It includes 'pure' parallel motions 'p', but also
        octaves 'o' and eventually unison 'u' which also imply parallelism.
        """
        # Check global attributes
        for attribute in ['p', 'o']:
            if attribute in self.global_attributes:
                return True
            # Check in layers and sublayers
            for layer in self.layers:
                if layer.has_attribute(attribute,
                        high_level_only=False):
                    return True
        # Else, no parallelism found
        return False

    def depth(self):
        """Return the maximal depth of sublayers in the texture"""
        return max([layer.depth() for layer in self.layers])

    def child_texture(self):
        """Return the texture after decomposing one level of layers

        For example, 2[M1/HS1(M1/S1)] will give 2[M1/M1/S1]
        """
        child_layers = []
        for layer in self.layers:
            if layer.is_terminal():
                # Keep the layer
                child_layers.append(layer)
            else:
                # Decompose the layer
                child_layers += layer.sublayers
        child_notation = (
            str(self.global_density)
            + ''.join(self.global_attributes)
            + '[' + '/'.join([str(layer) for layer in child_layers]) + ']'
        )
        return Texture(child_notation)

    def tree(self):
        """Display the texture as a tree of layers"""
        if not HAS_MATPLOTLIB:
            raise ModuleNotFoundError("matplotlib module is necessary to run this function.")
        fig, ax = plt.subplots()

        # Build the hierarchy of layers and sublayers
        layers_hierarchy = [self.layers]
        current_level_str = self.notation
        sub_level_texture = self.child_texture()
        while sub_level_texture.notation != current_level_str:
            # Continue until the layers cannot be decomposed...
            layers_hierarchy.append(sub_level_texture.layers)
            # Update current level of hierarchy
            current_level_str = sub_level_texture.notation
            sub_level_texture = sub_level_texture.child_texture()

        # Plot the links between the nodes
        x_left_layer = 0
        x_interval = 10
        y_left_interval = 100 / (len(layers_hierarchy[0])+1)
        for level_counter in range(1, len(layers_hierarchy)):
            y_left_layer = 100
            y_right_interval = 100 / (len(layers_hierarchy[level_counter])+1)
            y_right_layer = 100 - y_right_interval
            # Iterate on the parent (left-hand) layers
            for layer in layers_hierarchy[level_counter-1]:
                y_left_layer -= y_left_interval
                # Iterate on the parent (left-hand) layers
                for child_index in range(len(layer)):
                    ax.plot(
                        [x_left_layer, x_left_layer + x_interval],
                        [y_left_layer, y_right_layer],
                        color='black',  markersize=15)
                    y_right_layer -= y_right_interval
            # Update coordinates for the next iteration
            x_left_layer += x_interval
            y_left_interval = y_right_interval

        # Create the labels of the nodes
        x_layer = 0
        for layers in layers_hierarchy:
            y_layer = 100
            y_interval = 100/(len(layers)+1)
            for layer in layers:
                y_layer -= y_interval
                ax.text(
                    x_layer, y_layer,
                    layer.high_level(),
                    ha="center", va="center",
                    size=15, bbox=dict(boxstyle="round"))
            x_layer += x_interval

        # Set dimension, hide axes
        ax.set_xlim([
            -4,
            x_interval*(len(layers_hierarchy)-1) + 4
        ])
        ax.set_xticks([])
        ax.set_ylim([0, 100])
        ax.set_yticks([])
        ax.set_title(self.notation)
        plt.show()
